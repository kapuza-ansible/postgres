# PostreSQL install in Ubuntu role
Last check in Ubuntu 22.04.03 at 2025-01-15

Default install potgresql-14

## Use example
```bash
# Clone
git clone git@gitlab.com:kapuza-ansible/postgres.git

# Cd to example
cd example

# Clone libs role
cd roles
git clone git@gitlab.com:kapuza-ansible/libs.git
cd ..

# Copy config
cp roles/postgres/defaults/main.yml inventory/group_vars/postgres.yml

# Edit config
vim inventory/group_vars/postgres.yml

# Edit inventory
vim inventory/hosts

# Check install and config
ansible-playbook ./postgres_10_install.yml --check

# Start install and config
ansible-playbook ./postgres_10_install.yml

# If you want reinstal and restart
vim inventory/group_vars/postgres.yml
ansible-playbook ./postgres_20_config.yml --check
ansible-playbook ./postgres_20_config.yml
```

